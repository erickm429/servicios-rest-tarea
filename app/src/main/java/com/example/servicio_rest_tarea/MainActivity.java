package com.example.servicio_rest_tarea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.uoc.android.restservice.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
